# README #

### What is this repository for? ###

* Flicker Search App 
to search for photo at flicker public api, with ability to get more photos of the selected user's photo, the search results are cached using volley
* Version 1.0

### How do I get set up? ###

* Dependencies 
volley project folder should be added to /flicker-search/FlickerApp/libs, you can get it from 
"git clone https://android.googlesource.com/platform/frameworks/volley"

### App flavors ###
* The app has two flavors (demo and full), 
the only difference between them is app style
choose app flavor from Build variants then clean and rebuild the app before running it

### For more information contact me ###

* Dina El-Shaer
* dina.elshare@gmail.com