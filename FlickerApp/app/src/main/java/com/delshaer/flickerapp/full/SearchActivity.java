package com.delshaer.flickerapp.full;

import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.MenuItem;

import com.delshaer.flickerapp.full.utilities.Constants;

public class SearchActivity extends ParentActivity {

    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        initialize(R.string.search_activity_title, true);

        searchView = (SearchView) findViewById(R.id.search_view);
        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search(query);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    private void search(String query){
        String url = String.format(Constants.URL_SEARCH_PHOTOS, Constants.API_KEY, query, Constants.PER_PAGE, 1);
        loadData(url);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


}
