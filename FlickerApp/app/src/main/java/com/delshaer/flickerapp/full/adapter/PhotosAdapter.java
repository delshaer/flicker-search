package com.delshaer.flickerapp.full.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.delshaer.flickerapp.full.R;
import com.delshaer.flickerapp.full.UserPhotosActivity;
import com.delshaer.flickerapp.full.model.Photo;
import com.delshaer.flickerapp.full.utilities.VolleySingleton;

import java.util.LinkedList;

/**
 * Created by delshaer on 16/11/15.
 */
public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> {
    private LinkedList<Photo> mDataset;
    private ImageLoader mImageLoader;
    private boolean clickable;
    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public NetworkImageView mThumbnailImageView;
        public View view;
        public ViewHolder(View v) {
            super(v);
            view = v;
            mTextView = (TextView) v.findViewById(R.id.textView_title);
            mThumbnailImageView = (NetworkImageView) v.findViewById(R.id.imageView_thumbnail);
        }
    }

    public void setData(LinkedList<Photo> mDataset) {
        this.mDataset = mDataset;
    }

    public PhotosAdapter(LinkedList<Photo> mDataset,Context mContext, boolean clickable) {
        this.mDataset = mDataset;
        this.mContext = mContext;
        this.clickable = clickable;
        mImageLoader = VolleySingleton.getInstance(mContext).getImageLoader();
    }

    @Override
    public PhotosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_photo, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        String text = mDataset.get(position).getTitle();
        if(text.length() > 40){
            text = text.substring(0,40) + "...";
        }
        holder.mTextView.setText(text);
        holder.mThumbnailImageView.setImageUrl(mDataset.get(position).getPhotoThumbnailURL(), mImageLoader);
        holder.mThumbnailImageView.setErrorImageResId(R.drawable.ic_photo_black_48dp);
        holder.mThumbnailImageView.setDefaultImageResId(R.drawable.ic_photo_black_48dp);

        if(clickable){
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, UserPhotosActivity.class);
                    i.putExtra("owner", mDataset.get(position).getOwner());
                    mContext.startActivity(i);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}