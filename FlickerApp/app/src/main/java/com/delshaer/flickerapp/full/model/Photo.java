package com.delshaer.flickerapp.full.model;

/**
 * Created by delshaer on 15/11/15.
 */
public class Photo {

    private String id;
    private String owner;
    private String secret;
    private String server;
    private String farm;
    private String title;
    private String ispublic;
    private String isfriend;
    private String isfamily;

    public String getFarm() {
        return farm;
    }

    public String getId() {
        return id;
    }

    public String getIsfamily() {
        return isfamily;
    }

    public String getIsfriend() {
        return isfriend;
    }

    public String getIspublic() {
        return ispublic;
    }

    public String getOwner() {
        return owner;
    }

    public String getSecret() {
        return secret;
    }

    public String getServer() {
        return server;
    }

    public String getTitle() {
        return title;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFarm(String farm) {
        this.farm = farm;
    }

    public void setIsfamily(String isfamily) {
        this.isfamily = isfamily;
    }

    public void setIsfriend(String isfriend) {
        this.isfriend = isfriend;
    }

    public void setIspublic(String ispublic) {
        this.ispublic = ispublic;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhotoThumbnailURL(){
        return "https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + "_t.jpg";
    }

    public String getPhotoX640URL(){
        return "https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + "_z.jpg";
    }

    public String getPhotoX800URL(){
        return "https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + "_c.jpg";
    }

    public String getPhotoX1024URL(){
        return "https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + "_b.jpg";
    }

    public String getPhotoX1600URL(){
        return "https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + "_h.jpg";
    }

    public String getPhotoX2048URL(){
        return "https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + "_k.jpg";
    }
}
