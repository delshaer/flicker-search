package com.delshaer.flickerapp.full;

import android.os.Bundle;
import android.view.MenuItem;

import com.delshaer.flickerapp.full.utilities.Constants;

public class UserPhotosActivity extends ParentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_photos);

        initialize(R.string.user_photos_activity_title, false);

        String url = String.format(Constants.URL_USER_PHOTOS,
                Constants.API_KEY, getIntent().getExtras().getString("owner"), Constants.PER_PAGE, 1);

        loadData(url);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
