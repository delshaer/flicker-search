package com.delshaer.flickerapp.full;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.delshaer.flickerapp.full.adapter.PhotosAdapter;
import com.delshaer.flickerapp.full.model.Photo;
import com.delshaer.flickerapp.full.utilities.ParseUtil;
import com.delshaer.flickerapp.full.utilities.VolleySingleton;
import com.github.rahatarmanahmed.cpv.CircularProgressView;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by delshaer on 17/11/15.
 */
public class ParentActivity extends AppCompatActivity  implements Response.ErrorListener, Response.Listener<String>{

    private CircularProgressView progressBar;
    private RecyclerView mRecyclerView;
    private PhotosAdapter mAdapter;
    private RelativeLayout parentView;

    private LinkedList<Photo> data = new LinkedList<>();

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    protected void initialize(int titleRes, boolean clickable){
        initializeToolbar(titleRes);
        initializeView(clickable);
    }

    private void initializeToolbar(int titleRes){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(titleRes);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initializeView(boolean clickable){
        parentView = (RelativeLayout) findViewById(R.id.main_layout);
        progressBar = (CircularProgressView) findViewById(R.id.progress_bar);

        mRecyclerView = (RecyclerView) findViewById(R.id.photos_recycle_view);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new PhotosAdapter(data, this, clickable);
        mRecyclerView.setAdapter(mAdapter);
    }

    protected void loadData(String url){
        data = new LinkedList<>();
        progressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.INVISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, this, this);
        RequestQueue queue = VolleySingleton.getInstance(getApplicationContext()).getRequestQueue();
        if(queue.getCache().get(url)!= null){
            String cachedResponse = new String(queue.getCache().get(url).data);
            updateView(cachedResponse);
        }
        queue.add(stringRequest);
    }

    private void updateView(String response){
        progressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        addData(ParseUtil.parsePhotos(response));
    }

    private void addData(ArrayList<Photo> newData){
        if(data.size() > 0) {
            boolean couldAdd = false;
            for (int i = newData.size() - 1; i >= 0; --i) {
                if(couldAdd){
                    data.addFirst(newData.get(i));
                }

                if (newData.get(i).getId().equals(data.get(0).getId())) {
                    couldAdd = true;
                }
            }
        }else{
            data.addAll(newData);
        }
        mAdapter.setData(data);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
        if(error instanceof com.android.volley.NoConnectionError){
            Snackbar.make(parentView, "No Internet Connection.", Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    @Override
    public void onResponse(String response) {
        updateView(response);
    }
}
