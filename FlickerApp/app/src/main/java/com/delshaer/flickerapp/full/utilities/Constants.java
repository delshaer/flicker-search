package com.delshaer.flickerapp.full.utilities;

/**
 * Created by delshaer on 16/11/15.
 */
public class Constants {

    public static final int PER_PAGE = 25;

    public static final String API_KEY = "8e732a4765deb39b88be34d245e8431d";
    public static final String URL_SEARCH_PHOTOS = "https://api.flickr.com/services/rest?method=flickr.photos.search&api_key=%1$s&text=%2$s&per_page=%3$s&privacy_filter=1&page=%4$s";
    public static final String URL_USER_PHOTOS = "https://api.flickr.com/services/rest?method=flickr.people.getPublicPhotos&api_key=%1$s&user_id=%2$s&per_page=%3$s&page=%4$s";

}
