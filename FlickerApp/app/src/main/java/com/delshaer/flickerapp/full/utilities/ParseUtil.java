package com.delshaer.flickerapp.full.utilities;

import android.util.Xml;

import com.delshaer.flickerapp.full.model.Photo;

import org.xmlpull.v1.XmlPullParser;

import java.io.StringReader;
import java.util.ArrayList;

/**
 * Created by delshaer on 16/11/15.
 */
public class ParseUtil {

    public static ArrayList<Photo> parsePhotos(String data){
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(new StringReader(data));
            parser.nextTag();
            int eventType = parser.getEventType();
            ArrayList<Photo> photos = new ArrayList();

            Photo mPhoto = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {

                String name = null;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("photo")) {
                            mPhoto = new Photo();
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("photo") && mPhoto != null) {
                            mPhoto.setId(parser.getAttributeValue("", "id"));
                            mPhoto.setOwner(parser.getAttributeValue("", "owner"));
                            mPhoto.setSecret(parser.getAttributeValue("", "secret"));
                            mPhoto.setServer(parser.getAttributeValue("", "server"));
                            mPhoto.setFarm(parser.getAttributeValue("", "farm"));
                            mPhoto.setTitle(parser.getAttributeValue("", "title"));
                            mPhoto.setIspublic(parser.getAttributeValue("", "ispublic"));
                            mPhoto.setIspublic(parser.getAttributeValue("", "isfriend"));
                            mPhoto.setIsfamily(parser.getAttributeValue("", "isfamily"));
                            photos.add(mPhoto);
                            mPhoto = null;
                        }
                        break;
                }
                eventType = parser.next();
            }
            return photos;
        } catch (Exception e){
        }
        return null;
    }


}
